#include "config.h"
#include "nrf24l01.h"
#include <unistd.h>
#include <wiringPi.h>

inline void nRF24L01_SPI1byte(unsigned char* spiByte){
    unsigned char i;
    for( i= 0; i < 8 ; i++){
            digitalWrite(MOSI,(*spiByte)&0x80 ? 1 : 0);
            usleep(1);
            digitalWrite(SCK,1);
            (*spiByte)<<=1;
            usleep(1);
            if(digitalRead(MISO)==1)(*spiByte)|=0x01;
            usleep(1);
            digitalWrite(SCK,0);
            usleep(1);
    }
}

void nRF24L01_commandSimple(unsigned char command){
    digitalWrite(CSN,0);
    usleep(1);
    nRF24L01_SPI1byte(&command);
    usleep(1);
    digitalWrite(CSN,1);
    nRF24L01_lastStatus=command;
}

void nRF24L01_commandData(unsigned char command, unsigned char * dataArray, unsigned char dataLength){
    unsigned char dataCounter;
    digitalWrite(CSN,0);
    usleep(1);
    nRF24L01_SPI1byte(&command);
    usleep(1);
    for( dataCounter=0;dataCounter<dataLength;dataCounter++){
        usleep(1);
        nRF24L01_SPI1byte(dataArray+dataCounter);
    }
    usleep(1);
    digitalWrite(CSN,1);
    nRF24L01_lastStatus=command;
}
