#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wiringPi.h>
#include <time.h>
#include <stdint.h>
#include "crypto.h"
#include "nrf24l01.h"
#include "config.h"


#define REPORT_POWERON  2
#define REPORT_MOTION   1
#define REPORT_STANDBY  0

#define SENSOR_ID       (encryptData[0]>>8)
#define BATTERY_LEVEL   (encryptData[0]&0x000f)
#define REPORT_SUBJECT  (encryptData[0]>>4)&0x000f
#define PACKET_COUNTER  encryptData[1]


uint16_t k[4]=encryption_key;


void initnRF24L01(void);


uint8_t data[5];          //communication buffer


int main() {


    printf("nRF24L01 test\n");

    wiringPiSetupGpio();
    pinMode(MOSI, OUTPUT);
    pinMode(MISO, INPUT);
    pinMode(SCK, OUTPUT);
    pinMode(CSN, OUTPUT);
    pinMode(CE, OUTPUT);

    initnRF24L01();

    printf("Initialization complete \n");
    //nRF24L01_R_REGISTER(nRF24L01_CONFIG,data,1);
    //printf("CONFIG : %X \n",data[0]);
    nRF24L01_R_REGISTER(nRF24L01_RX_ADDR_P1,data,5);
    printf("Listening on address : %X %X %X %X %X \n",data[0],data[1],data[2],data[3],data[4]);

    while(1){
        char *sensorStatus;
        time_t t;
        struct tm *tm;
        uint16_t encryptData[2];
        //Cleaning up status bits (else won't transmit)
        data[0] = 0b01110000;
        nRF24L01_W_REGISTER(nRF24L01_STATUS,data,1);

       // printf("Status Cleaned : %X\n",nRF24L01_lastStatus);

        usleep(500);

        digitalWrite(CE,1);
        //printf("Switched to RX Mode\n");

        //RX Settling time
        usleep(500);

        do{
            nRF24L01_NOP();
        }while((nRF24L01_lastStatus&0x40)!=0x40);

        digitalWrite(CE,0);
        //printf("Switched to Standby\n");

        usleep(500);

        nRF24L01_R_RX_PAYLOAD(data,4);

        //Decrypting
        encryptData[0]=((uint16_t)data[0]<<8)|data[1];
        encryptData[1]=((uint16_t)data[2]<<8)|data[3];
        decrypt(encryptData,k);


        switch(REPORT_SUBJECT){
        case REPORT_MOTION:
            sensorStatus="MOTION DETECTED !";
            break;
        case REPORT_STANDBY:
            sensorStatus="Standby";
            break;
        case REPORT_POWERON:
            sensorStatus="Power on complete !";
            break;
        default:
            sensorStatus="Sensor going crazy !";
            break;
        }

        time(&t);
        tm=localtime(&t);
        printf("[%02d:%02d:%02d] ",(*tm).tm_hour,(*tm).tm_min,(*tm).tm_sec);              //printing timestamp
        printf("RAW: 0x%02X%02X%02X%02X | ",data[0],data[1],data[2],data[3]);               //and raw data
        printf("ID: %2X | bat: %X | cpt: %05u | %s\n",SENSOR_ID,BATTERY_LEVEL, PACKET_COUNTER, sensorStatus);           //and decoded data
    }

    return 0;
}

void initnRF24L01(){

    digitalWrite(CE,0);

    //Powering down nRF24L01
    data[0]=0b00001000;
    nRF24L01_W_REGISTER(nRF24L01_CONFIG,data,1);

    //Flushing RX FIFO
    nRF24L01_FLUSH_RX();

    //Flushing TX FIFO
    nRF24L01_FLUSH_TX();

    //Disabling Enhanced ShockBurst
    data[0]=0b00000000;
    nRF24L01_W_REGISTER(nRF24L01_EN_AA,data,1);

    //Setting address widths of 3 bytes
    data[0]=0b00000001;
    nRF24L01_W_REGISTER(nRF24L01_SETUP_AW,data,1);

    //Disabling auto-retransmit
    data[0]=0b00000000;
    nRF24L01_W_REGISTER(nRF24L01_SETUP_RETR,data,1);

    //Setting communication channel
    data[0]=rf_channel;
    nRF24L01_W_REGISTER(nRF24L01_RF_CH,data,1);

    //Setting rf parameters
    data[0]=0b00000111;
    nRF24L01_W_REGISTER(nRF24L01_RF_SETUP,data,1);

    //Enabling Pipe1
    data[0]=0b00000011;
    nRF24L01_W_REGISTER(nRF24L01_EN_RXADDR,data,1);

    //Setting rx address
    data[0]=rx_address_byte0;
    data[1]=rx_address_byte1;
    data[2]=rx_address_byte2;
    data[3]=rx_address_byte3;
    data[4]=rx_address_byte4;
    nRF24L01_W_REGISTER(nRF24L01_RX_ADDR_P1,data,5);

    //Setting a payload size of 4 on pipe1
    data[0]=0x04;
    nRF24L01_W_REGISTER(nRF24L01_RX_PW_P1,data,1);

    //powering up nRF24L01, PRX, 2byte CRC
    data[0]=0b00001111;
    nRF24L01_W_REGISTER(nRF24L01_CONFIG,data,1);

    usleep(10000);
}
