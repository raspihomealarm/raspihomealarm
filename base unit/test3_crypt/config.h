#ifndef CONFIG_H
#define CONFIG_H

//pin map
#define MOSI    10
#define MISO    9
#define SCK     11
#define CSN     7
#define CE      25

#define encryption_key              {0x74cd,0xb825,0x7e93,0x1ba8}

#define rf_channel                  0x02       //default, from 0x00 to 0x7f


#define rx_address_byte0            0x42
#define rx_address_byte1            0x42
#define rx_address_byte2            0x42
#define rx_address_byte3            0x42
#define rx_address_byte4            0x42



#endif	/* CONFIG_H */

