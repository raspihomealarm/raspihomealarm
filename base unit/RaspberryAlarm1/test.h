#ifndef TEST_H
#define TEST_H

#include "rf/rfinterface.h"
#include "motionsensors/motionsensorsinterface.h"

#include <QTimer>
#include <QObject>
#include <QMap>

class Test : public QObject
{
    Q_OBJECT
public:
    explicit Test(QObject *parent = 0);

    void addSensorLost(uint sensorId);
    void badSensorCounter(uint sensorId);

    void nrfReady();
    void nrfError();

signals:

private:
    RfInterface* rfInterface;
    MotionSensorsInterface* motionSensorsInterface;

    QTimer* refreshTimer;

    void displaySensorsStatus();

    QMap<uint,uint> lostCounterMap;

};

#endif // TEST_H
