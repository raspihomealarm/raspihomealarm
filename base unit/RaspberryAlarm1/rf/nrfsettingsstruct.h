#ifndef NRFSETTINGSSTRUCT_H
#define NRFSETTINGSSTRUCT_H

#include <stdint-gcc.h>
#include "../settings.h"

struct NrfSettingsStruct {
    unsigned int spiChannel         = 0;
    //settings
    uint8_t addressPrefix[4]        = {0x00,0x00,0x00,0x00};
    uint8_t config                  = 0b00001000;
    uint8_t en_aa                   = 0b00000000;
    uint8_t en_rxaddr               = 0b00000000;
    uint8_t setup_aw                = 0b00000011;
    uint8_t setup_retr              = 0b00000000;
    uint8_t rf_ch                   = 0b00000010;
    uint8_t rf_setup                = 0b00001111;
    uint8_t dynpd                   = 0b00000000;
    uint8_t features                = 0b00000000;

    //RX addresses
    uint8_t rx_addr[PIPES_NUMBER]   = {0x00,0x00,0x00,0x00,0x00,0x00};

    //TX addresses
    uint8_t tx_addr[PIPES_NUMBER]   = {0x00,0x00,0x00,0x00,0x00,0x00};

    //RX payload length
    uint8_t rx_pw[PIPES_NUMBER]     = {0x00,0x00,0x00,0x00,0x00,0x00};
};

#endif // NRFSETTINGSSTRUCT_H
