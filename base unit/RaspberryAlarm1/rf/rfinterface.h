#ifndef RFMANAGER_H
#define RFMANAGER_H

#include "nrf24l01.h"
#include "rfcomworker.h"
#include "nrfsettingsstruct.h"

#include <QObject>
#include <QByteArray>
#include <QThread>
#include <QTimer>

#include <stdint-gcc.h>



class RfInterface : public QObject
{
    Q_OBJECT

public:
    explicit RfInterface(int newSpiChannel, QObject *parent = 0);
    ~RfInterface();

    void startCommunicationThread();
    void stopCommunicationThread();

    //config
    void setAddressPrefix(uint8_t addressPrefix[]);
    void setPipeTxAddress(uint pipeNumber, uint8_t address);
    void setPipeRxAddress(uint pipeNumber, uint8_t address);
    void setPipeRxEnabled(uint pipeNumber, bool enabled);
    void setPipeRxPayloadLength(uint pipeNumber, uint payloadLength);
    void setAddressWidth(uint addressWidth);
    void setRfChannel(uint channel);
    void setRfConfig(uint8_t config);
    void setRfSetup(uint8_t setup);

    void sendDataOnPipe0(QByteArray data);
    void sendDataOnPipe1(QByteArray data);
    void sendDataOnPipe2(QByteArray data);
    void sendDataOnPipe3(QByteArray data);
    void sendDataOnPipe4(QByteArray data);
    void sendDataOnPipe5(QByteArray data);

signals:
    void dataReceivedOnPipe0(QByteArray data);
    void dataReceivedOnPipe1(QByteArray data);
    void dataReceivedOnPipe2(QByteArray data);
    void dataReceivedOnPipe3(QByteArray data);
    void dataReceivedOnPipe4(QByteArray data);
    void dataReceivedOnPipe5(QByteArray data);

    void nrfReady();
    void nrfError();


    //do not use these
    void stopWorker();
    void sendData(unsigned int pipe, QByteArray data);

private:

    void dataReceived(unsigned int pipe, QByteArray data);
    uint clampPipeNumber(uint pipeNumber);

    bool canCreateNewWorker;

    NrfSettingsStruct settings;


};

#endif // RFMANAGER_H
