#include "rfcomworker.h"
#include "../settings.h"

#include <QDebug>

RfComWorker::RfComWorker(NrfSettingsStruct newSettings, QObject *parent) :
    QObject(parent)
{
    shouldStop=false;
    txList.clear();

    //ensuring settings won't enable reserved bits
    settings=newSettings;
    settings.config &= 0b01111100;
    settings.setup_aw &= 0b00000011;
    settings.rf_ch &= 0b01111111;
    settings.rf_setup &= 0b00011111;
    for(uint i=0;i<PIPES_NUMBER;i++){
        settings.rx_pw[i] &= 0b00111111;
        if(settings.rx_pw[i]>MAX_PAYLOAD_LEN)settings.rx_pw[i]=MAX_PAYLOAD_LEN;
    }
    settings.dynpd &= 0b00111111;
    settings.features &= 0b00000111;

    pollingTimer = new QTimer(this);
    connect(pollingTimer,&QTimer::timeout,this,&RfComWorker::poll);
    pollingTimer->setInterval(NRF_POLLING_INTERVAL);
    pollingTimer->setSingleShot(true);

    nrf = new Nrf24l01(settings.spiChannel);
}

void RfComWorker::sendData(unsigned int pipe, QByteArray data){
    if(pipe>=PIPES_NUMBER){
        qWarning()<<this->metaObject()->className()<<" : tried to write on non-existent pipe "<<QString::number(pipe);
    }
    else{
        txPacket packet;
        packet.pipeToUse=pipe;
        packet.data=data.left(MAX_PAYLOAD_LEN);
        txList.append(packet);
    }
}

void RfComWorker::start(){
    setup();
    check();
    poll();
}

void RfComWorker::stop(){
    shouldStop=true;
}

void RfComWorker::check(){
    QByteArray data;
    nrf->readRegister(nRF24L01_CONFIG,&data,1);
    if(((uint8_t)(data[0]))==(settings.config|PWR_UP|PRIM_RX)){
        emit nrfReady();
    }
    else{
        shouldStop=1;
        emit nrfError();
    }
}


void RfComWorker::poll(){
    if(shouldStop){
        //Powering down nRF24L01
        nrf->writeCEpin(0);
        QByteArray data;
        data[0]=settings.config;
        nrf->writeRegister(nRF24L01_CONFIG,&data,1);

        emit stopped();
    }
    else{
        receivePayload();
        transmitPayload();
        pollingTimer->start();
    }
}

void RfComWorker::receivePayload(){
    QByteArray data;
    nrf->nop();

    //Check if data ready to be read
    if( (  nrf->lastStatus() & RX_DR ) != RX_DR ) return;
    do{
        //Check on which pipe data where received
        uint8_t pipeToRead = (nrf->lastStatus() >> 1) & 0b00000111;

        //Clear RX_DR status bit
        data.clear();
        data[0]=RX_DR;
        nrf->writeRegister(nRF24L01_STATUS,&data,1);

        //If pipe number is inconsistent, return
        if(pipeToRead>=PIPES_NUMBER) return;

        //Read payload
        data.clear();
        nrf->readRxPayload(&data,settings.rx_pw[pipeToRead]);

        emit dataReceived(pipeToRead,data);

        //Check if more payload to Read
        data.clear();
        nrf->readRegister(nRF24L01_FIFO_STATUS,&data,1);
    }while( ((data[0]) & RX_EMPTY) != RX_EMPTY );
}

void RfComWorker::transmitPayload(){
    if(!txList.isEmpty()){
        txPacket packetToSend = txList.takeFirst();
        QByteArray data;

        //switching nrf to standby mode
        nrf->writeCEpin(0);

        //writing new tx address
        for(uint i=0;i<4;i++){
            data[i]=settings.addressPrefix[i];
        }
        data[5]=settings.tx_addr[packetToSend.pipeToUse];
        nrf->writeRegister(nRF24L01_TX_ADDR,&data,5);

        //writing payload
        nrf->writeTxPayload(&(packetToSend.data),packetToSend.data.length());

        //Switching to TX mode
        data[0]=settings.config|PWR_UP;
        nrf->writeRegister(nRF24L01_CONFIG,&data,1);

        //Transmitting
        nrf->writeCEpin(1);
        do{
            nrf->nop();
        }while((nrf->lastStatus() & TX_DS) != TX_DS);
        nrf->writeCEpin(0);

        //clearing TX_DS bit
        data[0]=TX_DS;
        nrf->writeRegister(nRF24L01_STATUS,&data,1);

        //switching back to RX mode
        data[0]=settings.config|PWR_UP|PRIM_RX;
        nrf->writeRegister(nRF24L01_CONFIG,&data,1);

        //Start receiving
        nrf->writeCEpin(1);
    }
}

void RfComWorker::setup(){
    nrf->writeCEpin(0);

    QByteArray data;

    //Powering down nRF24L01
    data[0]=settings.config;
    nrf->writeRegister(nRF24L01_CONFIG,&data,1);

    //Flushing FIFOs
    nrf->flushRx();
    nrf->flushTx();

    //Setting communication channel
    data[0]=settings.rf_ch;
    nrf->writeRegister(nRF24L01_RF_CH,&data,1);

    //Setting addresses width
    data[0]=settings.setup_aw;
    nrf->writeRegister(nRF24L01_SETUP_AW,&data,1);

    //Setting RF settings
    data[0]=settings.rf_setup;
    nrf->writeRegister(nRF24L01_RF_SETUP,&data,1);

    //Cleaning status
    data[0]=0b01110000;
    nrf->writeRegister(nRF24L01_STATUS,&data,1);

    //Setting Enhanced ShockBurst
    data[0]=settings.en_aa;
    nrf->writeRegister(nRF24L01_EN_AA,&data,1);

    //Setting auto-retransmit
    data[0]=settings.setup_retr;
    nrf->writeRegister(nRF24L01_SETUP_RETR,&data,1);

    //Setting rx address on pipe 0
    data[0]=settings.addressPrefix[0];
    data[1]=settings.addressPrefix[1];
    data[2]=settings.addressPrefix[2];
    data[3]=settings.addressPrefix[3];
    data[4]=settings.rx_addr[0];
    nrf->writeRegister(nRF24L01_RX_ADDR_P0,&data,5);

    //Setting rx address on pipe 1
    data[0]=settings.addressPrefix[0];
    data[1]=settings.addressPrefix[1];
    data[2]=settings.addressPrefix[2];
    data[3]=settings.addressPrefix[3];
    data[4]=settings.rx_addr[1];
    nrf->writeRegister(nRF24L01_RX_ADDR_P1,&data,5);

    //Setting rx address on pipe 2
    data[0]=settings.rx_addr[2];
    nrf->writeRegister(nRF24L01_RX_ADDR_P2,&data,1);

    //Setting rx address on pipe 3
    data[0]=settings.rx_addr[3];
    nrf->writeRegister(nRF24L01_RX_ADDR_P3,&data,1);

    //Setting rx address on pipe 4
    data[0]=settings.rx_addr[4];
    nrf->writeRegister(nRF24L01_RX_ADDR_P4,&data,1);

    //Setting rx address on pipe 5
    data[0]=settings.rx_addr[5];
    nrf->writeRegister(nRF24L01_RX_ADDR_P5,&data,1);

    //Setting a payload size on each pipe
    for(uint i=0;i<PIPES_NUMBER;i++){
        data[0]=settings.rx_pw[i];
        nrf->writeRegister(nRF24L01_RX_PW_P0+i,&data,1);
    }

    //Enabling Pipes
    data[0]=settings.en_rxaddr;
    nrf->writeRegister(nRF24L01_EN_RXADDR,&data,1);

    //Powering up nRF24L01, tx mode
    data[0]=settings.config|PWR_UP|PRIM_RX;
    nrf->writeRegister(nRF24L01_CONFIG,&data,1);

    //Start receiving
    nrf->writeCEpin(1);
}

RfComWorker::~RfComWorker(){
    nrf->writeCEpin(0);
    pollingTimer->stop();
    delete pollingTimer;
    delete nrf;
}

