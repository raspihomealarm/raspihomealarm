#include "rfinterface.h"
#include "../settings.h"

#include <QDebug>



RfInterface::RfInterface(int newSpiChannel, QObject *parent) :
    QObject(parent)
{
    canCreateNewWorker=true;
    settings.spiChannel=newSpiChannel;

}

//#########################################################################################################################################################
//##############################                                                                                             ##############################
//##############################                                     CONFIGURATION                                           ##############################
//##############################                                                                                             ##############################
//#########################################################################################################################################################



uint RfInterface::clampPipeNumber(uint pipeNumber){
    if(pipeNumber>=PIPES_NUMBER){
        pipeNumber=PIPES_NUMBER-1;
    }
    return pipeNumber;
}

void RfInterface::setAddressPrefix(uint8_t addressPrefix[]){
    for(uint i=0;i<sizeof(settings.addressPrefix);i++){
        settings.addressPrefix[i]=addressPrefix[i];
    }

}

void RfInterface::setPipeTxAddress(uint pipeNumber, uint8_t address){
    settings.tx_addr[clampPipeNumber(pipeNumber)]=address;
}

void RfInterface::setPipeRxAddress(uint pipeNumber, uint8_t address){
    settings.rx_addr[clampPipeNumber(pipeNumber)]=address;
}

void RfInterface::setPipeRxEnabled(uint pipeNumber, bool enabled){
    pipeNumber=clampPipeNumber(pipeNumber);
    if(enabled){
        settings.en_rxaddr|=(1<<pipeNumber);
    }
    else{
        settings.en_rxaddr&=(0<<pipeNumber);
    }
}

void RfInterface::setPipeRxPayloadLength(uint pipeNumber, uint payloadLength){
    settings.rx_pw[clampPipeNumber(pipeNumber)]=payloadLength&0b00111111;
}

void RfInterface::setAddressWidth(uint addressWidth){
    switch(addressWidth) {
        case 3:
            settings.setup_aw=0b00000001;
            break;
        case 4:
            settings.setup_aw=0b00000010;
            break;
        case 5:
            settings.setup_aw=0b00000011;
            break;
        default:
            qWarning()<<this->metaObject()->className()<<" : Wrong address width value required";
            break;
    }
}

void RfInterface::setRfChannel(uint channel){
    settings.rf_ch=channel&0b01111111;
}

void RfInterface::setRfConfig(uint8_t config){
    settings.config = config;
}

void RfInterface::setRfSetup(uint8_t setup){
    settings.rf_setup = setup;
}


//#########################################################################################################################################################
//##############################                                                                                             ##############################
//##############################                                   THREAD MANAGEMENT                                         ##############################
//##############################                                                                                             ##############################
//#########################################################################################################################################################

void RfInterface::startCommunicationThread(){
    if(canCreateNewWorker){

        RfComWorker* rfWorker = new RfComWorker(settings);
        QThread* workerThread = new QThread(this);

        rfWorker->moveToThread(workerThread);

        connect(rfWorker,&RfComWorker::dataReceived,this,&RfInterface::dataReceived);
        connect(this,&RfInterface::sendData,rfWorker,&RfComWorker::sendData);
        connect(this,&RfInterface::stopWorker,rfWorker,&RfComWorker::stop);

        connect(workerThread, &QThread::started,rfWorker,&RfComWorker::start);
        connect(rfWorker,&RfComWorker::stopped,workerThread,&QThread::quit);
        connect(rfWorker,&RfComWorker::stopped,rfWorker,&RfComWorker::deleteLater);
        connect(workerThread,&QThread::finished,rfWorker,&RfComWorker::deleteLater);

        connect(workerThread,&QThread::destroyed,[=]() { canCreateNewWorker=true; });

        connect(rfWorker, &RfComWorker::nrfReady,this,&RfInterface::nrfReady);
        connect(rfWorker, &RfComWorker::nrfError,this,&RfInterface::nrfError);

        workerThread->start();

        canCreateNewWorker=false;

    }
    else{
        qWarning()<<this->metaObject()->className()<<" : start called but already one worker !";
    }
}

void RfInterface::stopCommunicationThread(){
    emit stopWorker();
}

//#########################################################################################################################################################
//##############################                                                                                             ##############################
//##############################                                     DATA RECEPTION                                          ##############################
//##############################                                                                                             ##############################
//#########################################################################################################################################################


void RfInterface::dataReceived(unsigned int pipe, QByteArray data){
    switch(pipe){
    case 0:
        emit dataReceivedOnPipe0(data);
        break;
    case 1:
        emit dataReceivedOnPipe1(data);
        break;
    case 2:
        emit dataReceivedOnPipe2(data);
        break;
    case 3:
        emit dataReceivedOnPipe3(data);
        break;
    case 4:
        emit dataReceivedOnPipe4(data);
        break;
    case 5:
        emit dataReceivedOnPipe5(data);
        break;
    default:
        qWarning()<<this->metaObject()->className()<<" : Data received on non-existent pipe "<<QString::number(pipe);
    }
}

//#########################################################################################################################################################
//##############################                                                                                             ##############################
//##############################                                     DATA TRANSMISSION                                       ##############################
//##############################                                                                                             ##############################
//#########################################################################################################################################################

void RfInterface::sendDataOnPipe0(QByteArray data){
    emit sendData(0,data);
}
void RfInterface::sendDataOnPipe1(QByteArray data){
    emit sendData(1,data);
}
void RfInterface::sendDataOnPipe2(QByteArray data){
    emit sendData(2,data);
}
void RfInterface::sendDataOnPipe3(QByteArray data){
    emit sendData(3,data);
}
void RfInterface::sendDataOnPipe4(QByteArray data){
    emit sendData(4,data);
}
void RfInterface::sendDataOnPipe5(QByteArray data){
    emit sendData(5,data);
}



RfInterface::~RfInterface(){
    stopCommunicationThread();
}
