﻿#include "nrf24l01.h"

#include <unistd.h>
#include <wiringPiSPI.h>
#include <wiringPi.h>
#include <QtGlobal>
#include <QByteArray>
#include <QDebug>


Nrf24l01::Nrf24l01(int newSpiChannel)
{
    wiringPiSetupGpio();
    pinMode(CE_PIN,OUTPUT);

    spiChannel=newSpiChannel;
    wiringPiSPISetup(spiChannel,SPI_SPEED);

    nop();
}


uint8_t Nrf24l01::lastStatus(){
    return statusByte;
}

void Nrf24l01::writeCEpin(unsigned int value){
    digitalWrite(CE_PIN,value);
}

int Nrf24l01::readCEpin(){
    return digitalRead(CE_PIN);
}

void Nrf24l01::doSpiTransfer(uint8_t command, QByteArray *data, unsigned int length){
    bool deleteData = false;
    if(data==NULL){
        data = new QByteArray(0);
        deleteData=true;
    }
    if(length>0) data->resize(length);

    data->prepend(command);

    wiringPiSPIDataRW(spiChannel, (unsigned char*)data->data(), data->length()) ;

    statusByte=data->at(0);
    data->remove(0,1);

    if(deleteData) delete data;
}


void Nrf24l01::readRegister(uint8_t registerValue, QByteArray* data, unsigned int length){
    doSpiTransfer(registerValue&0b00011111,data,length);
}

void Nrf24l01::writeRegister(uint8_t registerValue, QByteArray* data, unsigned int length){
    doSpiTransfer((registerValue&0b00011111)|0b00100000,data,length);
}

void Nrf24l01::readRxPayload(QByteArray *data, unsigned int length){
    doSpiTransfer(0b01100001,data,length);
}

void Nrf24l01::writeTxPayload(QByteArray* data, unsigned int length){
    doSpiTransfer(0b10100000,data,length);
}

void Nrf24l01::flushTx(){
    doSpiTransfer(0b11100001);
}

void Nrf24l01::flushRx(){
    doSpiTransfer(0b11100010);
}

void Nrf24l01::reuseTxPayload(){
    doSpiTransfer(0b11100011);
}

void Nrf24l01::activate(uint8_t param){
    QByteArray array((char*)&param);
    doSpiTransfer(0b01010000,&array,1);
}

void Nrf24l01::readRxPayloadWidth(){
    doSpiTransfer(0b01100000);
}

void Nrf24l01::writeAckPayload(uint8_t pipe, QByteArray *data, unsigned int length){
    doSpiTransfer(pipe&0b00000111,data,length);
}

void Nrf24l01::writeTxPayloadNoAck(){
    doSpiTransfer(0b10110000);
}

void Nrf24l01::nop(){
    doSpiTransfer(0xff);
}

Nrf24l01::~Nrf24l01(){
}


