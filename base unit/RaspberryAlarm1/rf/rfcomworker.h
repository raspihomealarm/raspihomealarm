#ifndef RFCOMWORKER_H
#define RFCOMWORKER_H

#include <QObject>
#include <QByteArray>
#include <QTimer>
#include <QList>

#include "nrf24l01.h"
#include "nrfsettingsstruct.h"



struct txPacket{
    uint pipeToUse;
    QByteArray data;
};

class RfComWorker : public QObject
{
    Q_OBJECT
public:
    explicit RfComWorker(NrfSettingsStruct newSettings, QObject *parent = 0);
    ~RfComWorker();

    void start();
    void stop();
    void sendData(unsigned int pipe, QByteArray data);

signals:
    void stopped();

    void nrfReady();
    void nrfError();

    void dataReceived(unsigned int pipe, QByteArray data);

private:
    void poll();
    void setup();
    void check();

    void receivePayload();
    void transmitPayload();

    bool shouldStop;
    QTimer* pollingTimer;
    Nrf24l01* nrf;
    NrfSettingsStruct settings;
    QList<txPacket> txList;

};

#endif // RFCOMWORKER_H
