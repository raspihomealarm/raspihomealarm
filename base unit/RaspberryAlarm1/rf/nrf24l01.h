#ifndef NRF24L01_H
#define NRF24L01_H

#include <QString>
#include <stdint-gcc.h>

#define PIPES_NUMBER    6
#define SPI_SPEED       4000000
#define CE_PIN          25
#define MAX_PAYLOAD_LEN 32

//REGISTER LIST
#define nRF24L01_CONFIG         0x00
#define nRF24L01_EN_AA          0x01
#define nRF24L01_EN_RXADDR      0x02
#define nRF24L01_SETUP_AW       0x03
#define nRF24L01_SETUP_RETR     0x04
#define nRF24L01_RF_CH          0x05
#define nRF24L01_RF_SETUP       0x06
#define nRF24L01_STATUS         0x07
#define nRF24L01_OBSERVE_TX     0x08
#define nRF24L01_CD             0x09
#define nRF24L01_RX_ADDR_P0     0x0A
#define nRF24L01_RX_ADDR_P1     0x0B
#define nRF24L01_RX_ADDR_P2     0x0C
#define nRF24L01_RX_ADDR_P3     0x0D
#define nRF24L01_RX_ADDR_P4     0x0E
#define nRF24L01_RX_ADDR_P5     0x0F
#define nRF24L01_TX_ADDR        0x10
#define nRF24L01_RX_PW_P0       0x11
#define nRF24L01_RX_PW_P1       0x12
#define nRF24L01_RX_PW_P2       0x13
#define nRF24L01_RX_PW_P3       0x14
#define nRF24L01_RX_PW_P4       0x15
#define nRF24L01_RX_PW_P5       0x16
#define nRF24L01_FIFO_STATUS    0x17
#define nRF24L01_DYNPD          0x1C
#define nRF24L01_FEATURE        0x1D

//USEFULL BITS
#define PWR_UP      0b00000010
#define PRIM_RX     0b00000001

#define RX_DR       0b01000000
#define TX_DS       0b00100000
#define MAX_RT      0b00010000

#define TX_FULL     0b00100000
#define TX_EMPTY    0b00010000
#define RX_FULL     0b00000010
#define RX_EMPTY    0b00000001


class Nrf24l01
{
public:
    explicit Nrf24l01(int newSpiChannel);
    ~Nrf24l01();

    uint8_t lastStatus(void);

    void readRegister(uint8_t registerValue, QByteArray* data, unsigned int length = 0);
    void writeRegister(uint8_t registerValue, QByteArray* data, unsigned int length = 0);
    void readRxPayload(QByteArray *data, unsigned int length = 0);
    void writeTxPayload(QByteArray *data, unsigned int length = 0);
    void flushTx();
    void flushRx();
    void reuseTxPayload();
    void activate(uint8_t param);
    void readRxPayloadWidth();
    void writeAckPayload(uint8_t pipe, QByteArray* data, unsigned int length = 0);
    void writeTxPayloadNoAck();
    void nop();

    void writeCEpin(unsigned int value);
    int readCEpin();

private:
    void doSpiTransfer(uint8_t command, QByteArray* data = NULL, unsigned int length = 0);

    uint8_t statusByte;


    int spiChannel;
};

#endif // NRF24L01_H
