#ifndef CONFIG_H
#define CONFIG_H

#define RF_CHANNEL                              2
#define NRF_POLLING_INTERVAL                    10

#define ADDRESS_PREFIX                          {0x42,0x42,0x42,0x42}

#define ENCRYPTION_KEY                          {0x74cd,0xb825,0x7e93,0x1ba8}

#define MOTION_SENSOR_DEFAULT_TIMOUT_DELAY      60
#define MOTION_SENSOR_ALLOWED_COUNTER_WINDOW    360

#define SENSORS_BACKUP_FILE                     "knownSensors"
#define SENSORS_BACKUP_INTERVAL                 60
#define SENSORS_ID_NUMBERING_BASE               16

#endif // CONFIG_H
