#include "test.h"

#include <QDateTime>
#include <QDebug>
#include <QCoreApplication>

Test::Test(QObject *parent) :
    QObject(parent)
{
    rfInterface= new RfInterface(0,this);

    rfInterface->setRfChannel(RF_CHANNEL);
    rfInterface->setRfConfig(0b00001100);
    rfInterface->setRfSetup(0b00000111);
    rfInterface->setAddressWidth(3);
    rfInterface->setPipeRxAddress(0,0x42);
    rfInterface->setPipeRxPayloadLength(0,4);
    rfInterface->setPipeRxEnabled(0,true);

    uint8_t addressPrefix[4]= ADDRESS_PREFIX;
    rfInterface->setAddressPrefix(addressPrefix);
    rfInterface->startCommunicationThread();

    motionSensorsInterface = new MotionSensorsInterface(this);
    //motionSensorsInterface->enableSensorDiscovery(true);

    connect(rfInterface,&RfInterface::dataReceivedOnPipe0,motionSensorsInterface,&MotionSensorsInterface::decodeSensorReport);
    connect(rfInterface,&RfInterface::nrfReady,this,&Test::nrfReady);
    connect(rfInterface,&RfInterface::nrfError,this,&Test::nrfError);

    connect(motionSensorsInterface,&MotionSensorsInterface::sensorLost,this,&Test::addSensorLost);
    connect(motionSensorsInterface,&MotionSensorsInterface::sensorBadCounter,this,&Test::badSensorCounter);


    refreshTimer = new QTimer(this);
    refreshTimer->setInterval(1000);

    connect(refreshTimer,&QTimer::timeout,this,&Test::displaySensorsStatus);

}

void Test::nrfReady(){
    qDebug()<<"NRF CHIP READY !";
    refreshTimer->start();
}

void Test::nrfError(){
    qCritical()<<"NRF CHIP CONFIG FAILED !";
    QCoreApplication::quit();
}

void Test::displaySensorsStatus(){
    QList<MotionSensorSummary> sensorsSummaryList = motionSensorsInterface->getSensorsSummary();

    QString currentLine;
    QString argument;

    int fieldLength=11;

    //first line
    currentLine="####################";
    for(int i = 0 ; i<sensorsSummaryList.size();i++){
        currentLine.append("##############");
    }
    qDebug()<<currentLine;

    //sensor ID line
    currentLine="# Sensor ID        #";
    for(int i = 0; i<sensorsSummaryList.size();i++){
        argument.setNum(sensorsSummaryList.at(i).id,SENSORS_ID_NUMBERING_BASE);
        argument.truncate(fieldLength);
        currentLine.append(" ");
        currentLine.append(argument);
        for(int j = 0; j< fieldLength-argument.length();j++ ){
            currentLine.append(" ");
        }
        currentLine.append(" #");
    }
    qDebug()<<currentLine;

    //sensor name line
    currentLine="# Sensor Name      #";
    for(int i = 0; i<sensorsSummaryList.size();i++){
        argument=sensorsSummaryList.at(i).name;
        argument.truncate(fieldLength);
        currentLine.append(" ");
        currentLine.append(argument);
        for(int j = 0; j< fieldLength-argument.length();j++ ){
            currentLine.append(" ");
        }
        currentLine.append(" #");
    }
    qDebug()<<currentLine;

    //battery level line
    currentLine="# Battery Level    #";
    for(int i = 0; i<sensorsSummaryList.size();i++){
        argument.setNum(sensorsSummaryList.at(i).batteryLevel);
        argument.truncate(fieldLength);
        currentLine.append(" ");
        currentLine.append(argument);
        for(int j = 0; j< fieldLength-argument.length();j++ ){
            currentLine.append(" ");
        }
        currentLine.append(" #");
    }
    qDebug()<<currentLine;

    //report subject line
    currentLine="# Report Subject   #";
    for(int i = 0; i<sensorsSummaryList.size();i++){
        switch(sensorsSummaryList.at(i).lastReportValue){
        case REPORT_MOTION:
            currentLine.append(" Motion      #");
            break;
        case REPORT_POWERON:
            currentLine.append(" Power on    #");
            break;
        case REPORT_STANDBY:
            currentLine.append(" Standby     #");
            break;
        case -1:
            currentLine.append(" Not init    #");
            break;
        default:
            currentLine.append(" DaFuck ??   #");
            break;
        }
    }
    qDebug()<<currentLine;

    //time elapsed since last report line
    currentLine="# Time Elapsed     #";
    QDateTime currentTime = QDateTime::currentDateTime();
    for(int i = 0; i<sensorsSummaryList.size();i++){
        int timeDiff = sensorsSummaryList.at(i).lastReportDateTime.secsTo(currentTime);
        currentLine.append(" ");
        if(timeDiff>600){
            currentLine.append("+10min     ");
        }
        else{
            argument.setNum(timeDiff);
            argument.truncate(fieldLength-2);
            argument.append(" s");
            currentLine.append(argument);
            for(int j = 0; j< fieldLength-argument.length();j++ ){
                currentLine.append(" ");
            }
        }
        currentLine.append(" #");
    }
    qDebug()<<currentLine;

    //Connection lost counter line
    currentLine="# Lost counter     #";
    for(int i = 0; i<sensorsSummaryList.size();i++){
        int sensorId = sensorsSummaryList.value(i).id;
        if(lostCounterMap.contains(sensorId)){
            argument.setNum(lostCounterMap.value(sensorId));
        }
        else{
            argument.setNum(0);
        }
        argument.truncate(fieldLength);
        currentLine.append(" ");
        currentLine.append(argument);
        for(int j = 0; j< fieldLength-argument.length();j++ ){
            currentLine.append(" ");
        }
        currentLine.append(" #");
    }
    qDebug()<<currentLine;

    //last line
    currentLine="####################";
    for(int i = 0 ; i<sensorsSummaryList.size();i++){
        currentLine.append("##############");
    }
    qDebug()<<currentLine;
}


void Test::addSensorLost(uint sensorId){
    if(lostCounterMap.contains(sensorId)){
        lostCounterMap[sensorId]++;
    }
    else{
        lostCounterMap.insert(sensorId,1);
    }
}

void Test::badSensorCounter(uint sensorId){
    qDebug()<<"sensor "<<QString::number(sensorId,16)<<" bad counter";
}
