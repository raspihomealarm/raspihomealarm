#include "settings.h"
#include "test.h"

#include <stdint-gcc.h>

#include <QCoreApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Test* test = new Test();
    Q_UNUSED(test);

    return a.exec();
}
