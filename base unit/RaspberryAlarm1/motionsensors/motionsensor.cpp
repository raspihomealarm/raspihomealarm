#include "motionsensor.h"
#include "../settings.h"

#include <QDebug>


MotionSensor::MotionSensor(uint newId, QObject *parent) :
    QObject(parent)
{
    id = newId;

    //init members to blank values
    lastBatteryLevel = -1;
    lastReportDateTime = QDateTime();
    lastReportValue = -1;

    lastPacketCounterValue = 0;
    doNotCheckPacketCounterValue = true;

    isConnectionLost = false;

    createConnectAndStartTimer();
}

MotionSensor::MotionSensor(uint newId, QString newName, int newBatteryLevel, int newReportValue,int packetCounterValue, QDateTime newReportDateTime, QObject *parent):
    QObject(parent)
{
    id=newId;
    name=newName;

    lastBatteryLevel=newBatteryLevel;
    lastReportValue=newReportValue;
    lastReportDateTime = newReportDateTime;

    lastPacketCounterValue = packetCounterValue;
    doNotCheckPacketCounterValue = true;

    isConnectionLost = false;

    createConnectAndStartTimer();
}

void MotionSensor::createConnectAndStartTimer(){
    connectionLostTimer = new QTimer(this);
    connectionLostTimer->setInterval(MOTION_SENSOR_DEFAULT_TIMOUT_DELAY*1000);
    connectionLostTimer->setSingleShot(true);

    connect(connectionLostTimer,&QTimer::timeout,this,&MotionSensor::connectionLostTimerTimeout);

    connectionLostTimer->start();
}

void MotionSensor::updateValues(uint batteryLevel, uint reportValue, uint packetCounterValue){
    if( ((lastPacketCounterValue < packetCounterValue) &&  (packetCounterValue <= (lastPacketCounterValue + MOTION_SENSOR_ALLOWED_COUNTER_WINDOW))) || doNotCheckPacketCounterValue){
        lastBatteryLevel=(int)batteryLevel;
        lastReportValue=(int)reportValue;
        lastPacketCounterValue=packetCounterValue;

        lastReportDateTime=QDateTime::currentDateTime();

        connectionLostTimer->start();

        doNotCheckPacketCounterValue = false;

        if(reportValue == REPORT_MOTION) emit triggered(id);
    }
    else{
        emit packetCounterRejected(id);
    }
}

void MotionSensor::setName(QString newName){
    name = newName;
}

MotionSensorSummary MotionSensor::getSummary(void){
    MotionSensorSummary summary;
    summary.id=this->id;
    summary.name=this->name;
    summary.batteryLevel=this->lastBatteryLevel;
    summary.lastReportValue=this->lastReportValue;
    summary.lastReportDateTime=this->lastReportDateTime;
    summary.lastPacketCounterValue=this->lastPacketCounterValue;
    summary.connectionLost=this->isConnectionLost;
    return summary;
}

void MotionSensor::disablePacketCounterValueCheck(bool disable){
    doNotCheckPacketCounterValue = disable;
}

void MotionSensor::setConnectionLostDelay(uint delay){
    connectionLostTimer->setInterval(delay);
}

void MotionSensor::connectionLostTimerTimeout(){
    doNotCheckPacketCounterValue = false;
    isConnectionLost=true;
    emit connectionLost(id);
}


MotionSensor::~MotionSensor(){
    delete connectionLostTimer;
}
