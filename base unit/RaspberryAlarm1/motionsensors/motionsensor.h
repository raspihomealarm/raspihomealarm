#ifndef MOTIONSENSOR_H
#define MOTIONSENSOR_H

#include <QObject>
#include <QTimer>
#include <QDateTime>
#include <QString>

#include <stdint-gcc.h>

#define REPORT_POWERON  2
#define REPORT_MOTION   1
#define REPORT_STANDBY  0

struct MotionSensorSummary{
    uint id;
    QString name;
    int batteryLevel;
    int lastReportValue;
    int lastPacketCounterValue;
    QDateTime lastReportDateTime;
    bool connectionLost;
};


class MotionSensor : public QObject
{
    Q_OBJECT
public:
    explicit MotionSensor(uint newId, QObject *parent = 0);
    explicit MotionSensor(uint newId, QString newName, int newBatteryLevel, int newReportValue, int packetCounterValue, QDateTime newReportDateTime, QObject *parent = 0);
    ~MotionSensor();

    void updateValues(uint batteryLevel, uint reportValue, uint packetCounterValue);
    void setConnectionLostDelay(uint delay);
    void setName(QString newName);
    void disablePacketCounterValueCheck(bool disable);

    MotionSensorSummary getSummary(void);

signals:
    void connectionLost(uint sensorId);
    void triggered(uint sensorId);
    void packetCounterRejected(uint sensorId);

private:
    uint id;
    QString name;
    int lastBatteryLevel;
    int lastReportValue;
    uint lastPacketCounterValue;
    bool isConnectionLost;

    bool doNotCheckPacketCounterValue;

    QTimer* connectionLostTimer;
    QDateTime lastReportDateTime;

    void connectionLostTimerTimeout();

    void createConnectAndStartTimer();
};

#endif // MOTIONSENSOR_H
