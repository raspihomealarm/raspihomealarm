#ifndef SENSORSINTERFACE_H
#define SENSORSINTERFACE_H

#include "motionsensor.h"

#include <QObject>
#include <QByteArray>
#include <QMap>
#include <QList>
#include <QString>
#include <QSettings>
#include <QTimer>

#include <stdint-gcc.h>


#include <QDebug>

#define SENSORS_MSG_LEN     4

class MotionSensorsInterface : public QObject
{
    Q_OBJECT
public:
    explicit MotionSensorsInterface(QObject *parent = 0);
    ~MotionSensorsInterface();

    void decodeSensorReport(QByteArray data);

    void enableSensorDiscovery(bool enable);

    void forgetSensor(uint sensorId);

    void setSensorName(uint sensorId, QString name);

    QList<MotionSensorSummary> getSensorsSummary(void);

signals:
    void sensorLost(uint sensorId);
    void sensorTriggered(uint sensorId);
    void sensorBadCounter(uint sensorId);

    void sensorDicovered(uint sensorId);
    void sensorForgotten(uint sensorId);

private:
    bool sensorDiscoveryEnabled;
    QMap<uint, MotionSensor*> sensorsMap;
    QSettings* sensorsBackup;
    QTimer* autoSaveTimer;

    void connectAndInsertNewSensor(uint sensorId, MotionSensor* newSensor);

    void restoreSensors();
    void saveSensors();
};

#endif // SENSORSINTERFACE_H
