#include "motionsensorsinterface.h"
#include "crypto.h"
#include "../settings.h"

#include <QStringList>
#include <QDateTime>
#include <QCoreApplication>



MotionSensorsInterface::MotionSensorsInterface(QObject *parent) :
    QObject(parent)
{
    sensorDiscoveryEnabled = false;

    sensorsBackup = new QSettings(QCoreApplication::applicationDirPath()+"/"+SENSORS_BACKUP_FILE,QSettings::IniFormat);


    //restoring sensorsmap
    if(sensorsBackup->status() == QSettings::NoError){
        restoreSensors();
    }

    autoSaveTimer = new QTimer(this);
    autoSaveTimer->setInterval(SENSORS_BACKUP_INTERVAL*1000);
    connect(autoSaveTimer,&QTimer::timeout,this,&MotionSensorsInterface::saveSensors);
    autoSaveTimer->start();

}

void MotionSensorsInterface::connectAndInsertNewSensor(uint sensorId, MotionSensor *newSensor){
    connect(newSensor,&MotionSensor::connectionLost,this,&MotionSensorsInterface::sensorLost);
    connect(newSensor,&MotionSensor::triggered,this,&MotionSensorsInterface::sensorTriggered);
    connect(newSensor,&MotionSensor::packetCounterRejected,this,&MotionSensorsInterface::sensorBadCounter);
    sensorsMap.insert(sensorId,newSensor);
}

void MotionSensorsInterface::decodeSensorReport(QByteArray data){
    //decryption key
    static uint16_t encryptionKey[4]=ENCRYPTION_KEY;

    //Checking if data has correct length
    if(data.length() != SENSORS_MSG_LEN)   return;

    //building correct data format
    uint16_t dataToDecrypt[2];
    dataToDecrypt[0]=((uint16_t)data[0]<<8)|(uint16_t)data[1];
    dataToDecrypt[1]=((uint16_t)data[2]<<8)|(uint16_t)data[3];

    //decrypting
    decrypt(dataToDecrypt,encryptionKey);

    //decoding values
    uint sensorId = dataToDecrypt[0]>>8;
    uint batteryLevel = dataToDecrypt[0]&0x000f;
    uint reportValue  = (dataToDecrypt[0]>>4)&0x000f;
    uint packetCounter = dataToDecrypt[1];

    //creating new sensor
    if(sensorDiscoveryEnabled && !sensorsMap.contains(sensorId) && (reportValue  == REPORT_POWERON) && (packetCounter == 0)){
        MotionSensor* newSensor = new MotionSensor(sensorId,this);
        connectAndInsertNewSensor(sensorId,newSensor);
        emit (sensorDicovered(sensorId));
    }

    //updating sensor with received values
    if(sensorsMap.contains(sensorId)){
        sensorsMap.value(sensorId)->updateValues(batteryLevel,reportValue ,packetCounter);
    }
}


//allow unknown sensor discovery and know sensor packet counter sync
void MotionSensorsInterface::enableSensorDiscovery(bool enable){
    sensorDiscoveryEnabled = enable;
    MotionSensor* sensor;
    foreach(sensor, sensorsMap.values()){
        sensor->disablePacketCounterValueCheck(sensorDiscoveryEnabled);
    }
}

void MotionSensorsInterface::setSensorName(uint sensorId, QString name){
    if(sensorsMap.contains(sensorId)){
        sensorsMap.value(sensorId)->setName(name);
    }
    else{
        qWarning()<<this->metaObject()->className()<<" : tried to change name of non-existent sensor "<<QString::number(sensorId);
    }
}

void MotionSensorsInterface::forgetSensor(uint sensorId){
    if(sensorsMap.contains(sensorId)){
        delete sensorsMap.value(sensorId);
        sensorsMap.remove(sensorId);
        emit sensorForgotten(sensorId);
    }
    else{
        qWarning()<<this->metaObject()->className()<<" : tried to remove non-existent sensor "<<QString::number(sensorId);
    }
}

QList<MotionSensorSummary> MotionSensorsInterface::getSensorsSummary(void){
    QList<MotionSensorSummary> sensorsSummarylist;
    MotionSensor* sensor;
    foreach(sensor, sensorsMap.values()){
        sensorsSummarylist.append(sensor->getSummary());
    }
    return sensorsSummarylist;
}


void MotionSensorsInterface::restoreSensors(){
    QStringList knownSensors = sensorsBackup->childGroups();
    QString sensorIdString;
    foreach(sensorIdString, knownSensors){
        bool ok;
        uint sensorId = sensorIdString.toInt(&ok,SENSORS_ID_NUMBERING_BASE);
        if(ok){
            sensorsBackup->beginGroup(sensorIdString);
            QString name = sensorsBackup->value("name").toString();
            int batteryLevel = sensorsBackup->value("battery").toInt();
            int lastReportValue = sensorsBackup->value("reportValue").toInt();
            int lastPacketCounterValue = sensorsBackup->value("packetCounter").toInt();
            QDateTime lastReportDateTime = QDateTime::fromString(sensorsBackup->value("lastReportDateTime").toString());

            MotionSensor* newSensor = new MotionSensor(sensorId,name,batteryLevel,lastReportValue, lastPacketCounterValue, lastReportDateTime,this);
            connectAndInsertNewSensor(sensorId,newSensor);

            sensorsBackup->endGroup();
        }
    }
}

void MotionSensorsInterface::saveSensors(){
    QList<MotionSensorSummary> sensorsSummaryList = getSensorsSummary();
    MotionSensorSummary sensor;
    foreach(sensor, sensorsSummaryList){
        sensorsBackup->beginGroup(QString::number(sensor.id,SENSORS_ID_NUMBERING_BASE));
        sensorsBackup->setValue("name",sensor.name);
        sensorsBackup->setValue("battery",sensor.batteryLevel);
        sensorsBackup->setValue("reportValue",sensor.lastReportValue);
        sensorsBackup->setValue("packetCounter",sensor.lastPacketCounterValue);
        sensorsBackup->setValue("lastReportDateTime",sensor.lastReportDateTime.toString());
        sensorsBackup->endGroup();
    }
    sensorsBackup->sync();

}

MotionSensorsInterface::~MotionSensorsInterface(){
    delete autoSaveTimer;

    saveSensors();

    delete sensorsBackup;

    uint sensorId;
    foreach(sensorId, sensorsMap.keys()){
        delete sensorsMap.value(sensorId);
        sensorsMap.remove(sensorId);

    }
}
