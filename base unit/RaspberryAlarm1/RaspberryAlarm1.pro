#-------------------------------------------------
#
# Project created by QtCreator 2014-08-02T18:49:43
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = RaspberryAlarm1
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += c++11

TEMPLATE = app

INSTALLS += target

target.path = /home/pi/projects/alarme/testQt1
INCLUDEPATH += /mnt/rasp-pi-rootfs/usr/local/qt5pi/include/


SOURCES += main.cpp \
    rf/nrf24l01.cpp \
    rf/rfcomworker.cpp \
    rf/rfinterface.cpp \
    motionsensors/motionsensorsinterface.cpp \
    motionsensors/motionsensor.cpp \
    test.cpp

HEADERS += \
    settings.h \
    rf/nrf24l01.h \
    rf/rfcomworker.h \
    rf/nrfsettingsstruct.h \
    rf/rfinterface.h \
    motionsensors/motionsensorsinterface.h \
    motionsensors/crypto.h \
    motionsensors/motionsensor.h \
    test.h


#WiringPI
unix:!macx: LIBS += -L/mnt/rasp-pi-rootfs/usr/local/lib/ -lwiringPi

INCLUDEPATH += /mnt/rasp-pi-rootfs/usr/local/include
DEPENDPATH += /mnt/rasp-pi-rootfs/usr/local/include
