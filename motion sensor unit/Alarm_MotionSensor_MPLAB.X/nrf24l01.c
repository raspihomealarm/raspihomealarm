#include "config.h"
#include "nrf24l01.h"
#include <xc.h>

inline void nRF24L01_SPI1byte(uint8_t* spiByte){
    for(uint8_t i = 0; i < 8 ; i++){
            MOSI = (*spiByte)&0x80 ? 1 : 0;
            SCK = 1;
            (*spiByte)<<=1;
            if(MISO)(*spiByte)|=0x01;
            SCK=0;
    }
}

void nRF24L01_commandSimple(uint8_t command){
    CSN = 0;
    nRF24L01_SPI1byte(&command);
    CSN = 1;
    nRF24L01_lastStatus=command;
}

void nRF24L01_commandData(uint8_t command, uint8_t * dataArray, uint8_t dataLength){
    CSN = 0;
    nRF24L01_SPI1byte(&command);
    for(uint8_t dataCounter=0;dataCounter<dataLength;dataCounter++){
        nRF24L01_SPI1byte(dataArray+dataCounter);
    }
    CSN = 1;
    nRF24L01_lastStatus=command;
}

void nRF24L01_transmitOnePayload(void){
    CE=1;
    __delay_us(20);
    CE=0;
}