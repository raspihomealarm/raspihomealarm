#include <xc.h>
#include <stdint.h>
#include "config.h"
#include "nrf24l01.h"
#include "crypto.h"




#define REPORT_POWERON  2
#define REPORT_MOTION   1
#define REPORT_STANDBY  0


void initnRF24L01(void);
void measureBatteryLevel(void);
void sendReportToMainUnit(unsigned char MotionDetected);

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! REMEMBER TO TURN ON MCLR !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

uint8_t data[5];          //communication buffer
uint8_t batteryLevel;
uint16_t key[4]=encryption_key;

void main() {
    //configuring hardware
    PCON=0x03;              // 37 kHz oscillator
    CMCON=0x07;             // comparators disabled
    CSN = 1 ; SCK = 0 ; CE=0;    // initializing nRF24L01 pins

    leds(1,1);

    TRISA=0b00100101;       // configuring PORTA. All unused pins are configured as outputs to limit current consuption
    TRISB=0b00100001;       // configuring PORTB. All unused pins are configured as outputs to limit current consuption
    
    //configuring nRF24L01
    initnRF24L01();

    //sending first report
    measureBatteryLevel();
    sendReportToMainUnit(REPORT_POWERON);

    //configuring timout counter
    reset_TMR1();
    T1CON=0x21;             //TMR1 freerunning, prescaler 1/4
    PIE1=0x01;              //TMR1 interrupt enabled

    //configuring motion sensor interrupt
    OPTION_REG=0xff;        //RB0 rising edge detection
    INTCON=0b11010000;      //RB0 & peripheral interrupts enabled        

    while(1){
        NOP();NOP();NOP();NOP();NOP();NOP();NOP();NOP();NOP();NOP();

    }
}

void initnRF24L01(){
    __delay_ms(15);         //nRF24L01 init delay

    //Flushing RX FIFO
    nRF24L01_FLUSH_RX();

    //Flushing TX FIFO
    nRF24L01_FLUSH_TX();

    //Disabling Enhanced ShockBurst
    data[0]=0b00000000;
    nRF24L01_W_REGISTER(nRF24L01_EN_AA,data,1);

    //Setting adress widths of 3 bytes
    data[0]=0b00000001;
    nRF24L01_W_REGISTER(nRF24L01_SETUP_AW,data,1);

    //Disabling auto-retransmit
    data[0]=0b00000000;
    nRF24L01_W_REGISTER(nRF24L01_SETUP_RETR,data,1);

    //Setting communication channel
    data[0]=rf_channel;
    nRF24L01_W_REGISTER(nRF24L01_RF_CH,data,1);

    //Setting rf parameters, 1 Mbps
    data[0]=0b00000111;
    nRF24L01_W_REGISTER(nRF24L01_RF_SETUP,data,1);

    //No RX Addresses set

    //Setting tx address
    data[0]=tx_address_byte0;
    data[1]=tx_address_byte1;
    data[2]=tx_address_byte2;
    data[3]=tx_address_byte3;
    data[4]=tx_address_byte4;
    nRF24L01_W_REGISTER(nRF24L01_TX_ADDR,data,5);

    //No RX pipe enabled
}

void interrupt interrupt_function(void){
    GIE=0;
    if(PIR1bits.TMR1IF){    //keep alive timeout
        reset_TMR1();
        if(!INTCONbits.INTF){
            sendReportToMainUnit(REPORT_STANDBY);
            measureBatteryLevel();
        }
        PIR1bits.TMR1IF=0;
    }
    if(INTCONbits.INTF){    //motion detected
        reset_TMR1();
        sendReportToMainUnit(REPORT_MOTION);
        measureBatteryLevel();
        INTCONbits.INTF=0;
    }
   
    GIE=1;
}

void sendReportToMainUnit(unsigned char reportSubject){
    uint16_t encryptData[2];
    static uint16_t counter = 0;

    //Drive Leds
    if(reportSubject==REPORT_MOTION){
        leds(0,1);
    }
    else{
        leds(1,0);
    }

    //Building encryption array
    encryptData[0]=((uint16_t)sensor_id<<8)|(uint16_t)(reportSubject<<4)|(uint16_t)(batteryLevel&0x0f);
    encryptData[1]=counter;

    //Encrypting
    encrypt(encryptData,&key);

    //Building payload
    data[0]=encryptData[0]>>8;
    data[1]=encryptData[0]&0x00ff;
    data[2]=encryptData[1]>>8;
    data[3]=encryptData[1]&0x00ff;

    //Cleaning up status bits (else won't transmit)
    data[4] = 0b01110000;
    nRF24L01_W_REGISTER(nRF24L01_STATUS,data+4,1);

    //Powering up nRF24L01, allowing TX-DS interrupt only, PTX mode, 2 bytes CRC
    data[4] = 0b01011110;
    nRF24L01_W_REGISTER(nRF24L01_CONFIG,data+4,1);
    __delay_ms(2);
    
    //Transmitting payload to nRF24L01
    nRF24L01_W_TX_PAYLOAD(data,payload_width);

    //transmitting everything in TX FIFO
    CE=1;
    __delay_us(200);
    do{
    }while(IRQ);
    CE=0;

    //Powering down nRF24L01
    data[0] = 0b00001000;
    nRF24L01_W_REGISTER(nRF24L01_CONFIG,data,1);

    counter++;

    leds(0,0);
}

void measureBatteryLevel(void){
    unsigned char result;
    CMCON=0x0A;             //enabling comparators, use internal voltage reference
    VRCON=0x8F;             //enabling voltage reference to max
    while((CMCONbits.C2OUT)&&((VRCON&0x0F)!=battery_min_level)){
        VRCON--;
    }
    VRCONbits.VREN=0;
    CMCON=0x07;             //disabling comparators to save power
    result=VRCON&0x0F;

    //normalizing battery level (0 to 7)
    if(result<battery_min_level)batteryLevel=0;
    else batteryLevel=result-battery_min_level;
}