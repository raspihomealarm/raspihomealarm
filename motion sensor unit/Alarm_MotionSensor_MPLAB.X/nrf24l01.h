/* 
 * File:   nrf24l01.h
 * Author: Mat
 *
 * Created on 1 ao�t 2013, 19:00
 */

#ifndef NRF24L01_H
#define	NRF24L01_H

#include <stdint.h>

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!                                                !!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!                LIST OF COMMANDS                !!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!                                                !!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#define nRF24L01_R_REGISTER(reg,data,len)       nRF24L01_commandData(reg&0b00011111,data,len)
#define nRF24L01_W_REGISTER(reg,data,len)       nRF24L01_commandData((reg&0b00011111)|0b00100000,data,len)
#define nRF24L01_R_RX_PAYLOAD(data,len)         nRF24L01_commandData(0b01100001,data,len)
#define nRF24L01_W_TX_PAYLOAD(data,len)         nRF24L01_commandData(0b10100000,data,len)
#define nRF24L01_FLUSH_TX()                     nRF24L01_commandSimple(0b11100001)
#define nRF24L01_FLUSH_RX()                     nRF24L01_commandSimple(0b11100010)
#define nRF24L01_REUSE_TX_PL()                  nRF24L01_commandSimple(0b11100011)
#define nRF24L01_ACTIVATE(data)                 nRF24L01_commandData(0b01010000,data,1)
#define nRF24L01_R_RX_PL_WID()                  nRF24L01_commandSimple(0b01100000)
#define nRF24L01_W_ACK_PAYLOAD(pipe,data,len)   nRF24L01_commandData((pipe&0b00000111)|0b10101000,data,len)
#define nRF24L01_W_TX_PAYLOAD_NOACK()           nRF24L01_commandSimple(0b10110000)
#define nRF24L01_NOP()                          nRF24L01_commandSimple(0xff)


//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!                                                !!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!                LIST OF REGISTERS               !!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!                                                !!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

//EXAMPLE : #define nRF24L01_NAME_OF_REGISTER       ADRESSE,SIZE
#define nRF24L01_CONFIG         0x00
#define nRF24L01_EN_AA          0x01
#define nRF24L01_EN_RXADDR      0x02
#define nRF24L01_SETUP_AW       0x03
#define nRF24L01_SETUP_RETR     0x04
#define nRF24L01_RF_CH          0x05
#define nRF24L01_RF_SETUP       0x06
#define nRF24L01_STATUS         0x07
#define nRF24L01_OBSERVE_TX     0x08
#define nRF24L01_CD             0x09
#define nRF24L01_RX_ADDR_P0     0x0A
#define nRF24L01_RX_ADDR_P1     0x0B
#define nRF24L01_RX_ADDR_P2     0x0C
#define nRF24L01_RX_ADDR_P3     0x0D
#define nRF24L01_RX_ADDR_P4     0x0E
#define nRF24L01_RX_ADDR_P5     0x0F
#define nRF24L01_TX_ADDR        0x10
#define nRF24L01_RX_PW_P0       0x11
#define nRF24L01_RX_PW_P1       0x12
#define nRF24L01_RX_PW_P2       0x13
#define nRF24L01_RX_PW_P3       0x14
#define nRF24L01_RX_PW_P4       0x15
#define nRF24L01_RX_PW_P5       0x16
#define nRF24L01_FIFO_STATUS    0x17
#define nRF24L01_DYNPD          0x1C
#define nRF24L01_FEATURE        0x1D

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!                                                !!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!             PUBLIC STUFF (FOR USE)             !!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!                                                !!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

unsigned char nRF24L01_lastStatus;
//contains nRF24L01 status register value. Updated after each command.

void nRF24L01_commandSimple(uint8_t command);
//handle simple command tranmission with nRF24L01

void nRF24L01_commandData(uint8_t command, uint8_t * dataArray,uint8_t dataLength);
//handle commands and data transmission with nRF24L01

void nRF24L01_transmitOnePayload(void);
//start the transmission of one payload (one TX FIFO level)

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!                                                !!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!           PRIVATE STUFF (NOT FOR USE)          !!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!                                                !!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


inline void nRF24L01_SPI1byte(unsigned char* spiByte);
//transmit 1 byte via SPI



#endif	/* NRF24L01_H */

