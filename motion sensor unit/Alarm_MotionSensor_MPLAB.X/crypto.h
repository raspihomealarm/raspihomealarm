#include <stdint.h>

void encrypt (uint16_t* v, uint16_t* k) {
    int8_t i;
    for (i=0; i<=3; i++) {                        /* basic cycle start */
        v[0] += (v[1]<<3) ^ (v[1] + k[i]) ^ (v[1]>>2);
        v[1] += (v[0]<<3) ^ (v[0] + k[i]) ^ (v[0]>>2);
    }                                              /* end cycle */
}


void decrypt (uint16_t* v, uint16_t* k) {
    int8_t i;
    for (i=3; i>=0; i--) {                         /* basic cycle start */
        v[1] -= (v[0]<<3) ^ (v[0] + k[i]) ^ (v[0]>>2);
        v[0] -= (v[1]<<3) ^ (v[1] + k[i]) ^ (v[1]>>2);
    }                                              /* end cycle */
}
