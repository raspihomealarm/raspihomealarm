/* 
 * File:   config.h
 * Author: Mat
 *
 * Created on 1 ao�t 2013, 19:29
 */

#ifndef CONFIG_H
#define	CONFIG_H

#pragma config FOSC = INTOSCIO  // Oscillator Selection bits (INTOSC oscillator: I/O function on RA6/OSC2/CLKOUT pin, I/O function on RA7/OSC1/CLKIN)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = ON      // Power-up Timer Enable bit (PWRT disabled)
#pragma config MCLRE = ON       // RA5/MCLR/VPP Pin Function Select bit (RA5/MCLR/VPP pin function is MCLR)
#pragma config BOREN = OFF      // Brown-out Detect Enable bit (BOD disabled)
#pragma config LVP = OFF        // Low-Voltage Programming Enable bit (RB4/PGM pin has digital I/O function, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EE Memory Code Protection bit (Data memory code protection off)
#pragma config CP = OFF         // Flash Program Memory Code Protection bit (Code protection off)

//pin map
#define MOSI    PORTAbits.RA7
#define MISO    PORTBbits.RB5
#define SCK     PORTBbits.RB6
#define CSN     PORTAbits.RA6
#define CE      PORTBbits.RB7
#define IRQ     PORTAbits.RA0

#define leds(x,y)    PORTBbits.RB4=x;PORTBbits.RB3=y;

/*
 *
 */
#define _XTAL_FREQ                  37000
#define battery_min_level           0x08
#define payload_width               4

#define reset_TMR1()                TMR1H=0x01;TMR1L=0x00


#define rf_channel                  0x02        //default, from 0x00 to 0x7f

#define encryption_key              {0x74cd,0xb825,0x7e93,0x1ba8}

#define sensor_id                   0x01

#define tx_address_byte0            0x42
#define tx_address_byte1            0x42
#define tx_address_byte2            0x42
#define tx_address_byte3            0x42
#define tx_address_byte4            0x42



#endif	/* CONFIG_H */

