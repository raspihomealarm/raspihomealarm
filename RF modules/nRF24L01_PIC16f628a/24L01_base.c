#include <16f628a.h>
#fuses INTRC_IO, NOWDT, NOPROTECT, NOMCLR, NOBROWNOUT, PUT
#use delay(clock=4000000)

#include <regs_16f628a.h>

#define TX_MOSI  PIN_B2    //Serial Slave Input.
#define TX_MISO  PIN_B7    //Serial Slave Output.
#define TX_IRQ   PIN_B0    //Interrup�ao de sa�da. Ativo baixo
#define TX_CE    PIN_B5    //Chip Enable. Ativa modo TX ou RX
#define TX_CSN   PIN_B3    //Chip Select Not. Seleciona o chip zero
#define TX_SCK   PIN_B6    //SPI Clock.

#define low  output_low
#define high output_high

#define RED_LED        PIN_A6
#define BLUE_LED       PIN_A4

unsigned int8 data;

void configure_transmitter(void);
void transmit_data(void);
void envia_byte(int8 dado);

/*#int_timer1
void timer_handle(){

   static int counter;
   counter++;
   if(counter==5){
      RA5=1;
      counter=0;
   }else{
      RA5=0;
   }
   TMR1L=0xAF;       //setup for count 50000
   TMR1H=0x3C;
   
}
*/
void main()
{
       
    delay_ms(1000);
    configure_transmitter();
    low(RED_LED);
    delay_ms(10);
    high(RED_LED);
    TRISB0=1;
    /*GIE=1;          //GIE enable interrupts
    PEIE=1;         //Peripheral ints
    PIE1=0x01;      //timer 1 overflow interrupt
    TMR1L=0xAF;       //setup for count 50000. 50000*8*0.5us=200000us=200ms
    TMR1H=0x3C;
    */
    
    while(1)
    {
        
        if(!input(PIN_B0)){
          delay_ms(30);
          while(!input(PIN_B0));
          transmit_data();
          low(RED_LED);
          delay_ms(10);
          high(RED_LED);
          delay_ms(100);
        }
    }
        
}


void envia_byte(int8 dado)
{
   unsigned int bit_ctr;   
   
      for(bit_ctr=0;bit_ctr<8;bit_ctr++)      // output 8-bit
      {
         if(dado & 0x80) 
            high(TX_MOSI);
         else 
            low(TX_MOSI);
         dado = (dado << 1);           // shift next bit into MSB..
         high(TX_SCK);                 // Set nRF24L01_SCK high..
         low(TX_SCK);                  // ..then set nRF24L01_SCK low again
      }

}

//This sends out the data stored in the data_array
//data_array must be setup before calling this function
void transmit_data(void)
{
    unsigned int8 data, cmd;
        
    low(TX_CSN);
    
    cmd = 0x27; //0b00100111. (STATUS)
    data = 0x7E; //0b01111110  clear previous ints
    
    envia_byte(cmd);
    envia_byte(data);
    
    high(TX_CSN); //pulse csn    
    low(TX_CSN);
    
/******************************************************************************/
    
    cmd = 0x20; //0b00100000. (CONFIG)
    data = 0x3A; //ob00111010. PWR_UP = 1, enable crc of 1 byte, PTX

    envia_byte(cmd);
    envia_byte(data);
    
    high(TX_CSN); //pulse csn    
    low(TX_CSN);

/******************************************************************************/

    //clear TX fifo
    //the data sheet says that this is supposed to come up 0 after POR, 
    //but that doesn't seem to be the case
    cmd = 0xE1; //0b11100001. (FLUSH_TX)
    
    envia_byte(cmd);
    
    high(TX_CSN); //pulse csn    
    low(TX_CSN);
    
/******************************************************************************/

    cmd = 0xA0; //write in the payload
    envia_byte(cmd);
    
    data = 0x34; // 1 byte payload
    envia_byte(data);
    
    high(TX_CSN);;
    
    //Pulse CE to start transmission
    high(TX_CE);
    delay_ms(1);
    low(TX_CE);
    

}




//2.4G Configuration - Transmitter
//This sets up one RF-24G for shockburst transmission
void configure_transmitter(void)
{
    unsigned int8 data, cmd;
        
    low(TX_CE); // to avoid undesired transmition 
    low(TX_CSN); //

    cmd = 0x20; //0b00100000. (CONFIG)
    data = 0x38; //0b00111000. PTX, CRC enabled, TX_DS on IRQ pin
    
    envia_byte(cmd);
    envia_byte(data);
    
    high(TX_CSN); //pulse csn    
    low(TX_CSN);
    
/******************************************************************************/
        
    cmd = 0x24;  //0b00100100. (SETUP_RTR)
    data = 0x00; //auto retransmit off
    
    envia_byte(cmd);
    envia_byte(data);
    
    high(TX_CSN); //pulse csn    
    low(TX_CSN);
    
/******************************************************************************/
    
    cmd = 0x23;  //SETUP_AW 0x03
    data = 0x03; //0b00000011. address width 5
    
    envia_byte(cmd);
    envia_byte(data);
    
    high(TX_CSN); //pulse csn    
    low(TX_CSN);
    
/******************************************************************************/
    
    cmd = 0x26;  //0b00100110. RF_SETUP 0x06
    data = 0x07; //0b00000111. 1Mbps, 0dBm, SETUP LNA GAIN
    
    envia_byte(cmd);
    envia_byte(data);
    
    high(TX_CSN); //pulse csn    
    low(TX_CSN);
    
/******************************************************************************/
    
    cmd = 0x25;  //RF_CH. 0x05
    data = 0x02; //FREQ. CHANNEL 2
    
    
    envia_byte(cmd);
    envia_byte(data);
    
    high(TX_CSN); //pulse csn    
    low(TX_CSN);
    
/******************************************************************************/
    
    cmd = 0x30;  //0b00110000. (TX_ADDR)
    data = 0xE7; //0b11100111 11100111 11100111 11100111 11100111 set address E7E7E7E7E7, also default.
    
    envia_byte(cmd);
    envia_byte(data); //address 5 bytes long
    envia_byte(data);
    envia_byte(data);
    envia_byte(data);
    envia_byte(data);
    
    
    high(TX_CSN); //pulse csn    
    low(TX_CSN);
    
/******************************************************************************/
    
    //disable auto-ack, RX mode
    //shouldn't have to do this, but it won't TX if you don't
    cmd = 0x21;  //(EN_AA)
    data = 0x00; //00
    
    envia_byte(cmd);
    envia_byte(data);
    
    high(TX_CSN); //pulse csn    
        
/******************************************************************************/
    
        
}
