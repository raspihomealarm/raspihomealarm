//Register address for 16F628a


//Bank 0
#byte TMR0=0x01
#byte PCL=0x02
#byte STATUS=0x03
   #bit C=STATUS.0
   #bit DC=STATUS.1
   #bit Z=STATUS.2
   #bit _PD=STATUS.3
   #bit _TO=STATUS.4
   
#byte FSR=0x04
#byte PORTA=0x05
   #bit RA0=PORTA.0
   #bit RA1=PORTA.1
   #bit RA2=PORTA.2
   #bit RA3=PORTA.3
   #bit RA4=PORTA.4
   #bit RA5=PORTA.5
   #bit RA6=PORTA.6
   #bit RA7=PORTA.7

#byte PORTB=0x06
   #bit RB0=PORTB.0
   #bit RB1=PORTB.1
   #bit RB2=PORTB.2
   #bit RB3=PORTB.3
   #bit RB4=PORTB.4
   #bit RB5=PORTB.5
   #bit RB6=PORTB.6
   #bit RB7=PORTB.7

#byte PCLATH=0x0A
#byte INTCON=0x0B
   #bit RBIF=INTCON.0
   #bit INTF=INTCON.1
   #bit T0IF=INTCON.2
   #bit RBIE=INTCON.3
   #bit INTE=INTCON.4
   #bit T0IE=INTCON.5
   #bit PEIE=INTCON.6
   #bit GIE=INTCON.7

#byte PIR1=0x0C
   #bit TMR1IF=PIR1.0
   #bit TMR2IF=PIR1.1
   #bit CCP1IF=PIR1.2
   
   #bit TXIF=PIR1.4
   #bit RCIF=PIR1.5
   #bit CMIF=PIR1.6
   #bit EEIF=PIR1.7

#byte TMR1L=0x0E
#byte TMR1H=0x0F
#byte T1CON=0x10
   #bit TMR1ON=T1CON.0
   #bit TMR1CS=T1CON.1
   #bit _T1SYNC=T1CON.2
   #bit T1OSCEN=T1CON.3
   #bit T1CKPS0=T1CON.4
   #bit T1CKPS1=T1CON.5
   
   
#byte TMR2=0x11
#byte T2CON=0x12
   #bit T2CKPS0=T2CON.0
   #bit T2CKPS1=T2CON.1
   #bit TMR2ON=T2CON.2
   #bit TOUTPS0=T2CON.3
   #bit TOUTPS1=T2CON.4
   #bit TOUTPS2=T2CON.5
   #bit TOUTPS3=T2CON.6
   

#byte CCPR1L=0x15
#byte CCPR1H=0x16
#byte CCP1CON=0x17
   #bit CCP1M0=CCP1CON.0
   #bit CCP1M1=CCP1CON.1
   #bit CCP1M2=CCP1CON.2
   #bit CCP1M3=CCP1CON.3
   #bit CCP1X=CCP1CON.4
   #bit CCP1Y=CCP1CON.5

#byte RCSTA=0x18
   #bit RX9D=RCSTA.0
   #bit OERR=RCSTA.1
   #bit FERR=RCSTA.2
   #bit ADEN=RCSTA.3
   #bit CREN=RCSTA.4
   #bit SREN=RCSTA.5
   #bit RX9=RCSTA.6
   #bit SPEN=RCSTA.7
   
#byte TXREG=0x19
#byte RCREG=0x1A

#byte CMCON=0x1F
   #bit CM0=CMCON.0
   #bit CM1=CMCON.1
   #bit CM2=CMCON.2
   #bit CIS=CMCON.3
   #bit C1INV=CMCON.4
   #bit C2INV=CMCON.5
   //#bit C1OUT=CMCON.6
   //#bit C2OUT=CMCON.7

//Bank 1
#byte OPTION_REG=0x81
   #bit PS0=OPTION_REG.0
   #bit PS1=OPTION_REG.1
   #bit PS2=OPTION_REG.2
   #bit PSA=OPTION_REG.3
   #bit T0SE=OPTION_REG.4
   #bit T0CS=OPTION_REG.5
   #bit INTEDG=OPTION_REG.6
   #bit _RBPU=OPTION_REG.7
   
#byte TRISA=0x85
   #bit TRISA0=TRISA.0
   #bit TRISA1=TRISA.1
   #bit TRISA2=TRISA.2
   #bit TRISA3=TRISA.3
   #bit TRISA4=TRISA.4
   #bit TRISA5=TRISA.5
   #bit TRISA6=TRISA.6
   #bit TRISA7=TRISA.7
   
#byte TRISB=0x86
   #bit TRISB0=TRISB.0
   #bit TRISB1=TRISB.1
   #bit TRISB2=TRISB.2
   #bit TRISB3=TRISB.3
   #bit TRISB4=TRISB.4
   #bit TRISB5=TRISB.5
   #bit TRISB6=TRISB.6
   #bit TRISB7=TRISB.7
   
#byte PIE1=0x8C
   #bit TMR1IE=PIE1.0 //TIMER 1 INT
   #bit TMR2IE=PIE1.1 //TIMER 2 INT
   #bit CCP1IE=PIE1.2 //CAPTURE OR COMPARE INT
   
   #bit TXIE=PIE1.4   //TX INT
   #bit RCIE=PIE1.5   //RX INT
   #bit CMIE=PIE1.6   //COMPARATOR INT
   #bit EEIE=PIE1.7
   
#byte PCON=0x8E

#byte PR2=0x92

#byte TXSTA=0x98
#byte SPBRG=0x99
#byte EEDATA=0x9A
#byte EEADR=0x9B
#byte EECON1=0x9C
#byte EECON2=0x9D
