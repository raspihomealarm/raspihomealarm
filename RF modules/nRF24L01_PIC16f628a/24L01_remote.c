#include <16f628a.h>
#fuses INTRC_IO, NOWDT, NOPROTECT, NOMCLR, NOBROWNOUT, PUT
#use delay(clock=4000000)

#include <regs_16f628a.h>
#define RX_MOSI  PIN_B2    //Serial Slave Input.
#define RX_MISO  PIN_B7    //Serial Slave Output.
#define RX_IRQ   PIN_B0    //Interrup�ao de sa�da. Ativo baixo
#define RX_CE    PIN_B5    //Chip Enable. Ativa modo TX ou RX
#define RX_CSN   PIN_B3    //Chip Select Not. Seleciona o chip zero
#define RX_SCK   PIN_B6    //SPI Clock.

#define low  output_low
#define high output_high

#define RED_LED        PIN_A6
#define BLUE_LED       PIN_A4

unsigned int8 payload;

void configure_RX(void);
void ler_RX(void);
void envia_byte(int8 dado);


void main()
{
    delay_ms(100);
    configure_RX();
    TRISA6=0; //RA6 saida
    RA6=0;
    delay_ms(10);
    RA6=1;
    TRISB0=1; //RB0 entrada
    
    
    
    while(1)
    {
       if(!RB0){
         delay_ms(20);
         ler_RX();
         if(payload==0x34){
           RA6=!RA6;
           payload=0;
           configure_RX();
         }
       }
    }

}


void envia_byte(int8 dado)
{
   unsigned int bit_ctr;   
   
      for(bit_ctr=0;bit_ctr<8;bit_ctr++)      // output 8-bit
      {
         if(dado & 0x80) 
            high(RX_MOSI);
         else 
            low(RX_MOSI);
         dado = (dado << 1);                 // shift next bit into MSB..
         high(RX_SCK);                   // Set nRF24L01_SCK high..
         low(RX_SCK);                  // ..then set nRF24L01_SCK low again
      }
                               // return read byte
}

//Configura RX
void configure_RX(void)
{
    unsigned int8 data, cmd;
    
    low(RX_CSN); //CLEAR TO START CONFIG 
    low(RX_CE);
    
    cmd = 0x20; //0b00100000 (CONFIG)
    data = 0x39; //0b00111001  RX_DR IRQ pin, CRC enabled 1 BYTE, PRX
    
    envia_byte(cmd);
    envia_byte(data);
    
    high(RX_CSN); //pulse csn    
    low(RX_CSN);
    
/******************************************************************************/
    
    cmd = 0x21; //(EN_AA)
    data = 0x00; //disable auto-ack for all channels   
    
    envia_byte(cmd);
    envia_byte(data);
    
    high(RX_CSN); //pulse csn    
    low(RX_CSN);
    
/******************************************************************************/
    
    cmd = 0x23; //(SETUP_AW)
    data = 0x03; //address width = 5 bytes
    
    envia_byte(cmd);
    envia_byte(data);
    
    high(RX_CSN); //pulse csn    
    low(RX_CSN);
    
/******************************************************************************/
    
    cmd = 0x26; //(RF_SETUP)
    data = 0x07; //0b00000111.FORCE PLL LOCK, RF POWER 0dBm, data rate = 1MB, SETUP LNA GAIN
    
    envia_byte(cmd);
    envia_byte(data);
    
    high(RX_CSN); //pulse csn    
    low(RX_CSN);
    
/******************************************************************************/

    cmd = 0x31; //0b00110001. (RX_PW_P0)
    data = 0x01; //1 byte payload.
    
    envia_byte(cmd);
    envia_byte(data);
    
    high(RX_CSN); //pulse csn    
    low(RX_CSN);
    
/******************************************************************************/

    cmd = 0x25; //(RF_CH)
    data = 0x02; // SET CHANNEL 2
    
    envia_byte(cmd);
    envia_byte(data);
    
    high(RX_CSN); //pulse csn    
    low(RX_CSN);
    
/******************************************************************************/

    cmd = 0x2A; //0b00101010. (RX_ADDR_P0)
    data = 0xE7; //E7E7E7E7E7
    
    envia_byte(cmd);
    envia_byte(data); //address 5 bytes long
    envia_byte(data);
    envia_byte(data);
    envia_byte(data);
    envia_byte(data);
    
    
    high(RX_CSN); //pulse csn    
    low(RX_CSN);
    
/******************************************************************************/
    
    cmd = 0x20; //(CONFIG)
    data = 0x3B; //0b00111011. RX_DR(IRQ), EN_CRC 1BYTE, PWR_UP=1, PRX
    
    envia_byte(cmd);
    envia_byte(data);
    
    high(RX_CSN);
    high(RX_CE);
    
/******************************************************************************/
    
}



//reset all ints
void ler_RX(void)
{
    unsigned int8 data, cmd, bit_ctr;
    
    payload = 0;
    
    cmd = 0x61; //0b01100001. Read RX payload
    
    low(RX_CSN);
    
    envia_byte(cmd);
    
    for(bit_ctr=0;bit_ctr<8;bit_ctr++)      // output 8-bit
      {
         payload = (payload << 1);           // shift next bit into MSB..
         high(RX_SCK);                   // Set nRF24L01_SCK high..
         if(input(RX_MISO)) payload |= 1;
         low(RX_SCK);                  // ..then set nRF24L01_SCK low again
      }
    
    high(RX_CSN); //Pulsa CSN
    low(RX_CSN);
    
/******************************************************************************/
    
    cmd = 0xE2; //Flush RX FIFO
    
    envia_byte(cmd);
    
    high(RX_CSN);
    low(RX_CSN);

/******************************************************************************/

    cmd = 0x27;  //(STATUS)
    data = 0x40; //0b01000000. CLEAR RX_DR INT
    
    envia_byte(cmd);
    envia_byte(data);
    
    high(RX_CSN);
    low(RX_CSN);

/******************************************************************************/
        
    high(RX_CSN);
    
}
